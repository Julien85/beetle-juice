USE recette;

CREATE TABLE administrateur(
    id_admin  INT     PRIMARY KEY     AUTO_INCREMENT,
    identifiant     VARCHAR(80)    NOT NULL    UNIQUE,
    mdp     VARCHAR(80)    NOT NULL
);

CREATE TABLE type_ingredient(
    id_type  INT     PRIMARY KEY     AUTO_INCREMENT,
    libelle     VARCHAR(80)    NOT NULL
);

CREATE TABLE ingredient(
    id_ingredient    INT   PRIMARY KEY     AUTO_INCREMENT,
    id_type  INT     NOT NULL,
    nom      VARCHAR(80)    NOT NULL,
    descriptions      TEXT    NOT NULL,
    photo      VARCHAR(255)    NOT NULL,
    FOREIGN KEY (id_type) REFERENCES type_ingredient(id_type)
);

CREATE TABLE recette(
    id_recette      INT   PRIMARY KEY   AUTO_INCREMENT,
    nom      VARCHAR(80)    NOT NULL,
    descriptions      TEXT    NOT NULL,
    photo      VARCHAR(255)    NOT NULL
);

CREATE TABLE ingredient_recette(
    id_recette          INT     NOT NULL,
    id_ingredient       INT     NOT NULL,
    nb_portion          INT     NOT NULL,
    PRIMARY KEY (id_recette, id_ingredient),
    FOREIGN KEY (id_recette) REFERENCES recette(id_recette),
    FOREIGN KEY (id_ingredient) REFERENCES ingredient(id_ingredient)
);

